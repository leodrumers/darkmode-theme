package com.leodrumers.broadcast

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var newsRecyclerView: RecyclerView
    private lateinit var newsAdapter: NewAdapter
    private val mData: ArrayList<NewsItem> by lazy { getMDataList() }
    private lateinit var fabSwitcher: FloatingActionButton
    private var isDark = false
    private lateinit var rootLayout : ConstraintLayout
    lateinit var preferences :MyPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        //Activity full screen
        fullScreenMode()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences = MyPreferences(this)
        isDark = preferences.isDark
        fabSwitcher = findViewById(R.id.fab_switcher)
        rootLayout = findViewById(R.id.root_layout)
        newsRecyclerView = findViewById(R.id.new_rv)
        newsAdapter = NewAdapter()
        setRecyclerView()
        setThemeState()
        fabSwitcher.setOnClickListener{ changeThemeMode() }

    }

    private fun setThemeState() {
        if(isDark){
            rootLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.black))
        }else{
            rootLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        }
    }

    private fun saveThemeModeInPreference(dark: Boolean) {
        preferences.isDark = dark
    }

    private fun changeThemeMode() {
        isDark = !isDark
        if(isDark){
            rootLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.black))
        }else{
            rootLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        }
        saveThemeModeInPreference(isDark)
        newsAdapter = NewAdapter()
        newsAdapter.submitList(mData)
        newsRecyclerView.adapter = newsAdapter
    }

    private fun setRecyclerView() {
        newsAdapter.submitList(mData)
        newsRecyclerView.layoutManager = LinearLayoutManager(this)
        newsRecyclerView.adapter = newsAdapter
    }
}
