package com.leodrumers.broadcast

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_news.view.*

class NewAdapter : ListAdapter<NewsItem,NewAdapter.NewsViewHolder>(NewsAdapterDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class NewsViewHolder private constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        companion object {
            fun from(parent: ViewGroup): NewsViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =  layoutInflater.inflate(R.layout.item_news,parent,false)
                return NewsViewHolder(binding)
            }
        }


        fun bind(newsItem: NewsItem)= with(itemView){
            img_user.animation = AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation)
            container.animation = AnimationUtils.loadAnimation(context,R.anim.fada_scale_animation)
            tv_title.text = newsItem.title
            tv_description.text = newsItem.content
            tv_date.text = newsItem.date
            img_user.setImageResource(newsItem.userPhoto)
            setDarkModeTheme(context)
        }

        private fun View.setDarkModeTheme(context: Context) {
            if (MyPreferences(context).isDark) {
                container.setBackgroundResource(R.drawable.card_bg_dark)
                tv_description.setTextColor(ContextCompat.getColor(context,R.color.black))
            }
        }
    }
}

class NewsAdapterDiff:DiffUtil.ItemCallback<NewsItem>() {
    override fun areItemsTheSame(oldItem: NewsItem, newItem: NewsItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: NewsItem, newItem: NewsItem): Boolean {
        return oldItem == newItem
    }

}