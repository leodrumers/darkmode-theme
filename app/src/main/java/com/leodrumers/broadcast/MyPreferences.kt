/*
 * Copyright (c) 2019. All rights reserved for leodrumers
 */

package com.leodrumers.broadcast

import android.content.Context
import android.preference.PreferenceManager

class MyPreferences(context: Context) {
    private val prefs= PreferenceManager.getDefaultSharedPreferences(context)
    var isDark:Boolean
        get() =prefs.getBoolean("isDark",false)
        set(value) =prefs.edit().putBoolean("isDark",value).apply()
}