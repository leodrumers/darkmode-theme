/*
 * Copyright (c) 2019. All rights reserved for leodrumers
 */

package com.leodrumers.broadcast

data class NewsItem(
    var id: Int,
    var title: String,
    var content: String,
    var date: String,
    var userPhoto: Int
)